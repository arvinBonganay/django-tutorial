from . import views
from django.urls import include, path

app_name = 'registration'
urlpatterns = [
	path('signup', views.SignUp.as_view(), name='signup'),
]

